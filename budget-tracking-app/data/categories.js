const categories = [
    {
        id: 'mc1',
        type: 'mainCategory',
        value: 'main',
        categorySelection: [
            {
                id: 'mc1-1',
                name: 'Income',
                value: 'income'
            },
            {
                id: 'mc1-2',
                name: 'Expenses',
                value: 'expenses'
            }
        ]
    },
    {
        id: 'mc1-1',
        type: 'subCategory',
        value: 'expenses',
        categorySelection: [
            {
                id: 'sc1-1',
                name: 'Food',
                value: 'food'
            },
            {
                id: 'sc1-2',
                name: 'Education',
                value: 'education'
            },
            {
                id: 'sc1-3',
                name: 'Transportation',
                value: 'transport'
            },
            {
                id: 'sc1-4',
                name: 'Groceries',
                value: 'groceries'
            },
            {
                id: 'sc1-5',
                name: 'Restaurant',
                value: 'restaurant'
            },
            {
                id: 'sc1-6',
                name: 'Health',
                value: 'health'
            },
            {
                id: 'sc1-7',
                name: 'Gifts',
                value: 'gifts'
            }
        ]
    },
    {
        id: 'mc1-2',
        type: 'subCategory',
        value: 'income',
        categorySelection: [
            {
                id: 'sc2-1',
                name: 'Salary',
                value: 'salary'
            },
            {
                id: 'sc2-2',
                name: 'Bonus',
                value: 'bonus'
            },
            {
                id: 'sc2-3',
                name: 'Gifts',
                value: 'gifts'
            },
            {
                id: 'sc2-4',
                name: 'Loan',
                value: 'loan'
            }
        ]
    }
]

export default { categories }

export function getSubCategory(mainCategory) {
    const subCategory = subCategory.find(mainCategory => mainCategory == type)
    return subCategory.Subcatecory
}