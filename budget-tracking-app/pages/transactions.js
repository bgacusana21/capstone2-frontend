import { useContext, useState, useEffect } from 'react'
import { Form, Button, Row, Col, Modal } from 'react-bootstrap'

import TransactionModal from '../components/TransactionModal'
import TransactionHistory from '../components/TransactionsHistory'

export default function transactions() {
  
    const [transactTypeCat, settransactTypeCat] = useState([])
    const [transactionType, setTransactionType] = useState('')
    const [mainData, setMainData] = useState('')

	const mainCat = transactTypeCat.map(data => {
        if(data.isActive){
            return (
                <option key={data.id} value={data.value}>{data.name}</option>
            )
        }

    })
    
    function getMainCategoryData(mainCat) {
        const mainCatData = transactTypeCat.map(data => {
            if(mainCat === data.value){
                setMainData(data)
            }
        })
    }
    
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/transactions/mainCategory`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            settransactTypeCat(data.map(data => data))
        })
        
    }, [])


    return (
        <React.Fragment>
            <h1>Transactions</h1>
            
            <div className="form-row">
                <div className="form-group col-md-6">
                    <label>What is your transaction?</label>
                    <select className="form-control" name="transactionType" onChange={e => {
                        setTransactionType(e.target.value)
                        getMainCategoryData(e.target.value)
                        }}>
                            <option selected>Please select 1</option>
                            {mainCat}
                    </select>
                </div>
            </div>
            <TransactionModal data={mainData} modalHeader={transactionType}></TransactionModal>
            

            <div className="mt-5">
                <TransactionHistory length='5' searchData='[]' />
            </div>
        </React.Fragment>
        
    )
}
