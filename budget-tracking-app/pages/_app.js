import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from '../components/NavBar'
import { UserProvider } from '../UserContext'
import "react-datepicker/dist/react-datepicker.css"

import '../styles/globals.css'
import '../styles/navbar.css'

function MyApp({ Component, pageProps }) {

    const [userInfo, setUserInfo] = useState([])
    const [userTransactions, setUserTransactions] = useState([])
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
        })


    const unsetUser = () => {
        localStorage.clear();

        setUser({
            email: null,
            isAdmin: null
        })
    }

    useEffect(() => {
        fetch('http://localhost:4000/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        if(data._id){
            setUser({
                id:data._id,
                isAdmin: data.isAdmin
            })
            setUserInfo(data)
            setUserTransactions(data.transaction)
        } else {
            setUser({
                email: null,
                id: null,
                isAdmin: null
            })
        }
        })
    }, [user.id])

    return (
        <React.Fragment>
            <UserProvider value={{user, setUser, unsetUser, userInfo, userTransactions}}>
                <NavBar />
                <Component {...pageProps} />
            </UserProvider>
        </React.Fragment>
    )

}

export default MyApp
