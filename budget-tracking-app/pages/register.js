import { useState, useEffect } from 'react'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Head from 'next/head'
import Swal from 'sweetalert2'

export default function register() {

    const [firstName, setFirstName] = useState('test')
    const [lastName, setLastName] = useState('test')
    const [mobileNo, setMobileNo] = useState('12345')
    const [email, setEmail] = useState('test@test')
    const [password1, setPassword1] = useState('test')
    const [password2, setPassword2] = useState('test')
    const [isActive, setIsActive] = useState(false)
    const [passwordMatch, setPasswordMatch] = useState(false)


    function registerUser(e){
        e.preventDefault()
        console.log(email)
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === false){
                fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data === true){
                        Router.push('/login')
                    }else{
                        console.log('error')
                        // Router.push('/error')
                    }
                })
            }else{
                console.log('error in email exist')
                // Router.push('/error')
            }
        })
    }

    function resetForm() {
        setFirstName('test')
        setLastName('test')
        setMobileNo('12345')
        setEmail('test@test')
        setPassword1('test')
        setPassword2('test')
    }

    useEffect(() => {

        if((password1 !== '' && password2 !== '') && (password2 === password1)){
            setPasswordMatch(true)
        }else{
            setPasswordMatch(false)
        }

    }, [password1, password2])

    useEffect(() => {
        console.log('for submission')
        if((firstName.length > 1) && (firstName.length > 1) && (lastName.length > 1) && (mobileNo.length > 10) && (email.length > 10) && (passwordMatch === true )) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, mobileNo, email, passwordMatch])

    return (
        <React.Fragment>
            <Head>
                <title>Register to Budget Tracking</title>
            </Head>
            <Row className="pt-5" onSubmit={(e) => registerUser(e)}>
                <Col xs={1} md={1}></Col>
                <Col xs={10} md={6}>
                    <Form>
                        <Row>
                            <Col>
                                <Form.Label>Full Name</Form.Label>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group controlId="firstName">
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter your first name" 
                                        value={firstName}
                                        onChange={e => setFirstName(e.target.value)}
                                        required
                                    />
                                    <Form.Text className="text-muted">
                                        First Name
                                    </Form.Text>
                                </Form.Group>   
                            </Col>
                            <Col>
                                <Form.Group controlId="lastName">
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter your first name" 
                                        value={lastName}
                                        onChange={e => setLastName(e.target.value)}
                                        required
                                    />
                                    <Form.Text className="text-muted">
                                        Last Name
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>

                        <Form.Group controlId="userMobileNo">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control 
                                    type="number"
                                    placeholder="Enter your mobile number"
                                    value={mobileNo}
                                    onChange={e => setMobileNo(e.target.value)}
                                    pattern="[0]{1}[9]{1}[0-9]{9}"
                                    required
                                />
                        </Form.Group>

                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                value={email} 
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            {/* <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text> */}
                        </Form.Group>

                        <Form.Group controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value={password1}
                                onChange={e => setPassword1(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password2">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Verify Password" 
                                value={password2}
                                onChange={e => setPassword2(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                {passwordMatch ? `Password Match` : `Password does not match`}
                            </Form.Text>
                        </Form.Group>

                        {/* conditionally render submit button based on isActive state */}
                        {isActive
                            ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                            : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                        }
                        <Button className="ml-2" variant="warning" type="reset" id="resetBtn" onClick={e => resetForm(e)}>Reset</Button>
                    </Form>
                </Col>
                <Col xs={1} md={5}></Col>
            </Row>
        </React.Fragment>
    )
}