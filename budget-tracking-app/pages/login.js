import { useState, useContext } from 'react'
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import Router from 'next/router'

import UserContext from '../UserContext'

export default function login() {

    const { setUser } = useContext(UserContext)
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState('')

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist') {
                    Swal.fire(
                        'Authentication Failed',
                        'User does not exist.',
                        'error'
                    )
                } else if (data.error === 'incorrect-password') {
                    Swal.fire(
                        'Authentication Failed',
                        'Password is incorrect.',
                        'error'
                    )
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have reigstered through a different login procedure',
                        'error'
                    )
                }
            }
           
        })
    }

    const authenticateGoogleToken = (response) => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'google-auth-error') {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'You may have reigstered through a different login procedure',
                        'error'
                    )
                } 
            }
        })

    }

    const retrieveUserDetails = (accessToken) => {

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id:data._id,
                isAdmin: data.isAdmin
            })
            Router.push('/')
        })
    }

    return (
        <React.Fragment>
                <Row>
                    <Col xs={10} md={5}>
                        <Card>
                            <Card.Header>Login to your account</Card.Header>
                            <Card.Body>
                                <Form onSubmit={e => authenticate(e)}>
                                    <Form.Group controlId="userEmail">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control 
                                            type="email" 
                                            placeholder="Enter email" 
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="password">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control 
                                            type="password" 
                                            placeholder="Password" 
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Button className="bg-primary" type="submit">
                                        Submit
                                    </Button>

                                    <GoogleLogin
                                    clientId="907217793776-nvnkmjhlpiv7fjkun25vhbo5mjnsv6ga.apps.googleusercontent.com"
                                    buttonText="Login"
                                    onSuccess={ authenticateGoogleToken }
                                    onFailure={ authenticateGoogleToken }
                                    cookiePolicy={ 'single_host_origin'}
                                    className="w-100 text-center d-flex justify-content-center"
                                    />

                                </Form>
                                </Card.Body>
                        </Card>
                    </Col>
                </Row>           
        </React.Fragment>

    )
}