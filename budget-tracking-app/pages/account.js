import { useContext, useState, useEffect } from 'react'
import { Form, Button, Row, Col, Modal } from 'react-bootstrap'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'

export default function transactions() {
    
    const [showForm, setShowForm] = useState(false)
    const { userInfo, userTransactions } = useContext(UserContext)
    const [newPassword1, setNewPassword1] = useState('')
    const [newPassword2, setNewPassword2] = useState('')
    const [oldPassword, setOldPassword] = useState('')
    const [passwordMatch, setPasswordMatch] = useState(false)

    const handleClose = () => {
        setShowForm(false)
        resetForm()
    }
	const handleShow = () => {
        setShowForm(true)	
    }

    function changePassword(e){
        e.preventDefault()
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/change`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                oldPassword: oldPassword,
                newPassword: newPassword1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                console.log('change password successful')
                resetForm()
                setShowForm(false)
            } else {
                Swal.fire(
                    `Change Password Failed!`,
                    'Please try again.'
                )
            }
        })
    }

    function resetForm() {
        setOldPassword('')
        setNewPassword1('')
        setNewPassword2('')
    }

    useEffect(() => {

        if((newPassword1 !== '' && newPassword2 !== '') && (newPassword2 === newPassword1)){
            setPasswordMatch(true)
        }else{
            setPasswordMatch(false)
        }

    }, [newPassword1, newPassword2])

    return (
        <React.Fragment>
            <h1>Account Management</h1>
            
            <Row className="justify-content-center">
                <Col xs={11} md={9}>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formFirstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="text" value={userInfo.firstName} readOnly />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formLastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type="text" value={userInfo.lastName} readOnly />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formEmail">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="email" value={userInfo.email} readOnly />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formMobileNo">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control type="text" value={userInfo.mobileNo} readOnly />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formAccountStatus">
                                <Form.Label>Account Status</Form.Label>
                                <Form.Control type="text" value={userInfo.userStatus} readOnly />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formRegistrationMethod">
                                <Form.Label>Registration Method</Form.Label>
                                <Form.Control type="text" value={userInfo.loginType} readOnly />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formTransactionCount">
                                <Form.Label>Transactions Recorded</Form.Label>
                                <Form.Control type="text" value={userTransactions.length} readOnly />
                            </Form.Group>

                        </Form.Row>
                        {(userInfo.loginType==='web')
                        ?<Button variant="primary" onClick={() => {handleShow()}}>Change Password</Button>
                        :
                        null
                        }
                    </Form>
                </Col>
            </Row>

            <Modal show={showForm} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Change Password</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col xs={8} md={8}>
                            <Form onSubmit={(e) => changePassword(e)}>
                                <Form.Group controlId="oldpassword">
                                    <Form.Label>Old Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Password" 
                                        value={oldPassword} 
                                        onChange={e => {setOldPassword(e.target.value)}} 
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="password1">
                                    <Form.Label>New Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Password" 
                                        value={newPassword1}
                                        onChange={e => setNewPassword1(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="password2">
                                    <Form.Label>Retype New Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Verify Password" 
                                        value={newPassword2}
                                        onChange={e => setNewPassword2(e.target.value)}
                                        required
                                    />
                                    <Form.Text className="text-muted">
                                        {passwordMatch ? `Password Match` : `Password does not match`}
                                    </Form.Text>
                                </Form.Group>
                                {passwordMatch
                                ? <Button variant="primary" type="submit" id="submitBtn">Change</Button>
                                : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                                }                                
                            </Form>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="bg-secondary" onClick={handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
        
    )
}
