import { useState, useEffect } from 'react'
import { Button, Table, Col, Row, Badge } from 'react-bootstrap'
import { getSubCategory } from '../data/categories'

import AddCategory from '../components/AddCategory'
import Swal from 'sweetalert2'

export default function addCategory() {

    const [categoryType, setCategoryType] = useState('')
    const [transactTypeCat, setTransactTypeCat] = useState([])
    const [subCat, setSubCat] = useState([])
    const [subCatInactive, setSubCatInactive] = useState([])
    const [addCategoryData, setAddCategoryData] = useState([])
    const [openModal, setOpenModal] = useState(false)
    const [mainCatValue, setMainCatValue] = useState('')

    const mainCat = transactTypeCat.map(data => {
		return (
            <React.Fragment>
                {data.isActive 
                ? 
                <tr key={data.id}>
                    <td>
                        {data.name}
                        <Badge 
                                pill 
                                as="button" 
                                variant="primary"
                                value={data.value}
                                onClick={() => {
                                    subCategoryInActiveList(data)
                                    subCategoryList(data)}} 
                            >
                                Select
                        </Badge>
                        <Badge 
                                pill 
                                as="button" 
                                variant="danger"
                                value="mainCategory"
                                onClick={() => 
                                    {
                                        deleteCat(data, subCategoryList=undefined)
                                    }} 
                            >
                                Deactivate
                        </Badge>
                    </td>
                </tr>
                : 
                null
                }

            </React.Fragment>

		)
    })

    const isNotActiveMainCat = transactTypeCat.map(data => {
		return (
            <React.Fragment>
                {data.isActive 
                ? 
                null
                : 
                <tr key={data.id}>
                    <td>
                        {data.name}
                        <Badge 
                                pill 
                                as="button" 
                                variant="success"
                                value={data.value}
                                onClick={() => 
                                    {
                                        deleteCat(data, subCategoryList=undefined)
                                    }} 
                            >
                                Activate
                        </Badge>
                    </td>
                </tr>
                }
            </React.Fragment>
		)
    })

    function addCategoryModal(data){
        setAddCategoryData({
            categoryType: data,
            name: categoryType,
            mainCatValue: mainCatValue
        })
    }

    function deleteCat(data, subCatList){
        let status = data.isActive ? false : true
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/transactions/${data.value}/${subCatList===undefined?'main':subCatList.value}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: status,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)            
        })

    }

    function subCategoryList(data) {

        setMainCatValue(data.value)
        const subCatList = transactTypeCat.find(list => list.value === data.value)
        console.log(subCatList)
        if (subCatList.categorySelection.length > 0) {
            setSubCat(subCatList.categorySelection.map(data => {
                return (
                    <React.Fragment>
                        {data.isActive 
                        ? 
                        <tr key={data.id}>
                            <td>
                                {data.name}
                                <Badge 
                                        pill 
                                        as="button" 
                                        variant="danger"
                                        value="subCategory"
                                        onClick={() => 
                                            {
                                                deleteCat(data, subCatList)
                                            }}  
                                    >
                                        Deactivate
                                </Badge>
                            </td>
                        </tr>
                        : 
                        null
                        }        
                    </React.Fragment>
                )
            }))
        } else {
            Swal.fire(
                `No Sub Category.`,
                'Please add a subcategory.'
            )
        }
    }

    function subCategoryInActiveList(data) {

        setMainCatValue(data.value)
        const subCatList = transactTypeCat.find(list => list.value === data.value)
        console.log(subCatList)
        if (subCatList.categorySelection.length > 0) {
            setSubCatInactive(subCatList.categorySelection.map(data => {
                return (
                    <React.Fragment>
                        {data.isActive 
                        ? 
                        null
                        : 
                        <tr key={data.id}>
                            <td>
                                {data.name}
                                <Badge 
                                        pill 
                                        as="button" 
                                        variant="success"
                                        value={data.value}
                                        onClick={() => 
                                            {
                                                deleteCat(data, subCatList)
                                            }} 
                                    >
                                        Activate
                                </Badge>
                            </td>
                        </tr>
                        }
                    </React.Fragment>
                )
            }))
        } 
    }

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/transactions/mainCategory`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setTransactTypeCat(data)            
        })
    }, [])
    
    return (
        <React.Fragment>
            <h1>Manage Categories</h1>
            <Row>
                <Col xs={5} md={5}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>
                                    Active Main Category
                                    <Badge 
                                        pill 
                                        as="button" 
                                        variant="primary"
                                        value="mainCategory"
                                        onClick={e => {
                                            setOpenModal(true)
                                            setCategoryType('Main Category')
                                            addCategoryModal(e.target.value)
                                        }}
                                    >
                                        Add
                                    </Badge>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                                {mainCat}                       
                        </tbody>
                    </Table>
                </Col>

                <Col xs={5} md={5}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>
                                    Sub Category
                                    <Badge 
                                        pill 
                                        as="button" 
                                        variant="primary"
                                        value="subCategory"
                                        onClick={e => {
                                            {mainCatValue===''
                                            ?
                                            Swal.fire(
                                                `Can't add a Sub Category`,
                                                'Please select one Main Category.'
                                            )
                                            :
                                            setOpenModal(true)
                                            setCategoryType('subCategory')
                                            addCategoryModal(e.target.value)
                                            }
                                        }}
                                    >
                                        Add
                                    </Badge>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                                {subCat}                       
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row>
                <Col xs={5} md={5}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>
                                    Not Active Main Category
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                                {isNotActiveMainCat}                       
                        </tbody>
                    </Table>
                </Col>
                <Col xs={5} md={5}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>
                                    Not Active Sub Category
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                                {subCatInactive}                       
                        </tbody>
                    </Table>
                </Col>
            </Row>
            {openModal
            ?
            <AddCategory data={addCategoryData} openModal={openModal} />
            :
            null
            }
            


        </React.Fragment>
    )
}