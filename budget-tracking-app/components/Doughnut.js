import { useEffect, useState} from 'react'
import { Doughnut } from 'react-chartjs-2'

export default function DoughnutChart({data}){
    
    const [dataLabel, setDataLabel] = useState([])
    const [datasetsData, setDatasetsData] = useState([])
    
    useEffect(() => {

        const getLabel = data.map(data => data.transactionType)
        const dummy = getLabel.splice(0, getLabel.length, ...(new Set(getLabel)))    
        
        let num
        let array = []
        for(num=0;num<getLabel.length;num++){
            let total=0
            data.map(data => {
                if(data.transactionType==getLabel[num]){
                    total+=data.amount
                }
            })
            array.push(Math.abs(total))
        }

        let arrayTotal = 0
        array.forEach(element => arrayTotal+=element)
        const arrayPercent = array.map(element => {
            return Number(element/arrayTotal).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2}); 
        })

        setDataLabel(getLabel)
        setDatasetsData(array)

    }, [data])
    
    return (
        <React.Fragment>
            
            <Doughnut
                data={
                    {
                        datasets: [{
                            data: [...datasetsData],
                            backgroundColor: ["black", "green", "red", "yellow", "pink", "orage", "blue", "violet", "white", "indigo"]
                        }],
                        labels: [...dataLabel]
                    }
                } redraw = { false }
            />
        </React.Fragment>

    )
}