import { useRef, useState, useEffect } from 'react'
import { Table, Button, Form, FormControl, Row, Col, Modal, Dropdown, DropdownButton } from 'react-bootstrap'

import DatePicker from 'react-datepicker'
import Moment from 'react-moment'
import moment from 'moment'

export default function NavBar({searchData, length}) {

    const [history, setHistory] = useState([])
    const [last10, setLast10] = useState([])
    const [array, setArray] = useState([])
    const [balanceArray, setBalanceArray] = useState([])
    const [searchDesc, setSearchDesc] = useState('')
    const refresh = useRef(0)
    const [visible, setVisible] = useState('')
    const [search, setSearch] = useState([])


    function addAmount(data) {
        
        data.map(transaction => {
            setArray(value => value.concat(transaction.amount))
        })
        
        refresh.current = refresh.current + 1
    }
    
    useEffect(() => {
        
        if (length==1000){
            setVisible("none")
        } else setVisible("initial")
        
    },[length])

    useEffect(() => {
        
        if(array.length>0){
            let bal = 0
            let num
            let balArray = []
            let sampleArray= []
            for(num=array.length-1;num>=0;num--){
                bal=bal+array[num]
                balArray.push(bal)
                sampleArray.push({...history[num], balance: bal})                
            }
            sampleArray.reverse()
            setBalanceArray(sampleArray)
            
            
        }

    }, [refresh.current])

    useEffect(() => {
        
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/transactions/history`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            const sorted = data.sort((a,b) => moment.utc(b.transactionTimestamp).diff(moment.utc(a.transactionTimestamp)))
            setHistory(data)
            setArray([])
            addAmount(data)

        })
    },[])
    
    useEffect(() => {

        const historyDetails = search.map(detail => {
             
            return (
                <tr key={detail._id}>
                    <td>{moment(detail.transactionTimestamp).format('MMM DD, YYYY h:mm:ss A')}</td>
                    <td>{detail.summary}</td>
                    <td>Php {detail.amount}</td>
                    <td>{detail.transactionType}</td>
                    <td>{detail.transactSubCat}</td>
                    <td>{detail.description}</td>
                    <td>Php {detail.balance}</td>
                </tr>
            )
        })
        const valueTest = history
        setLast10(historyDetails.slice(0,length))
    }, [search])

    useEffect(() => {
        console.log(searchData.length)
        if(searchData.length>0){
            console.log(searchData)
            console.log(searchData[0].startDate)
            console.log(searchData[0].endDate)
            console.log(balanceArray)
            const searchResult = balanceArray.filter(data =>  {

                return (
                    
                    (data.summary.toLowerCase().includes(searchData[0].keyword.toLowerCase())) && 
                    (data.transactionType.toLowerCase().includes(searchData[0].mainCat.toLowerCase())) &&
                    (data.transactSubCat.toLowerCase().includes(searchData[0].subCat.toLowerCase())) &&
                    (moment(data.transactionTimestamp)>=moment(searchData[0].startDate)) && 
                    (moment(data.transactionTimestamp)<=moment(searchData[0].endDate).add(1, 'days'))
                    
                    )
            })
            setSearch(searchResult)
        } else{
            const searchResult = balanceArray
            setSearch(searchResult)
        }
        
    },[searchData])

    useEffect(() => {
        const searchResult = balanceArray.filter(data =>  (data.summary.toLowerCase().includes(searchDesc.toLowerCase())))
        setSearch(searchResult)
    }, [balanceArray, searchDesc])

    return (
        <React.Fragment>
            
            <Row style={{display: `${visible}`}}>
                <Col>
                    <h3>Recent Transactions</h3>
                </Col>
                <Col>
                    <Form inline>
                        <Form.Label>Quick Search</Form.Label>
                        <FormControl type="text" onChange={e => setSearchDesc(e.target.value)} placeholder="Search" className="mr-sm-2" />
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Summary</th>
                                <th>Amount</th>
                                <th>Transaction</th>
                                <th>Sub Category</th>
                                <th>Description</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            {last10}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </React.Fragment>

    )
}

 