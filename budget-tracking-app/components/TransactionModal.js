import { useContext, useState, useEffect } from 'react'
import { Form, Button, Row, Col, Modal, Dropdown, DropdownButton } from 'react-bootstrap'
import Swal from 'sweetalert2'
import DatePicker from 'react-datepicker'
import Moment from 'react-moment'
import moment from 'moment'

export default function NavBar({data, modalHeader}) {

    const [showForm, setShowForm] = useState(false)
    const [selectedSubCat, setSelectedSubCat] = useState('')
    const [selectedSubName, setSelectedSubName] = useState('')
    const [subCat, setSubCat] = useState([])

    const [startDate, setStartDate] = useState(new Date())
    const [summary, setSummary] = useState('')
    const [description, setDescription] = useState('')
    const [amount, setAmount] = useState('')


    const handleClose = () => {
        setShowForm(false)
        setSelectedSubName('')
        resetForm()
    }
	const handleShow = () => {
        if ((data!=='') && (data.categorySelection.length > 0)){
            setShowForm(true)
        } else {
            Swal.fire(
                `Can't add transaction!`,
                'Select transaction type or add sub category.'
            )
        }
		
    }

    function resetForm() {
        setStartDate(new Date())
        setSummary('')
        setDescription('')
        setAmount('')
        setSelectedSubCat('')

    }


    function addTransaction(e) {
        e.preventDefault()

        if (selectedSubCat===''){
            Swal.fire(
                `Can't add transaction!`,
                'Select sub category button.'
            )
        } else{
            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/transactions/add`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    transactionType: modalHeader,
                    transactSubCat: selectedSubCat.value,
                    summary: summary,
                    description: description,
                    amount: data.isDebit?-amount:amount,
                    transactionTimestamp: startDate
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data){
                    resetForm()
                    setShowForm(false)
                    setSelectedSubName('')
                } else{
                    Swal.fire(
                        `Can't add transaction!`,
                        'Please try again.'
                    )
                }           
            })
        }
    }


    useEffect(() => {

        if((data!=='') && (data.categorySelection.length > 0)){

            const length = data.categorySelection.length
            const list = data.categorySelection.map(sub => {
                return (
                    <Row>
                        <Button className="info m-1" size="sm" onClick={e => setSelectedSubCat(sub)}>{sub.name}</Button>
                    </Row>
                )
            })
            setSubCat(list)
        }
    }, [data])



    return (
        <React.Fragment>
            <Button className="bg-success" onClick={() => {handleShow()}}>Show Form</Button>
            <Modal show={showForm} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Please select {modalHeader} category</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col xs={4} md={4}>
                 
                        {subCat}

                        </Col>
                        <Col xs={8} md={8}>
                            <Form onSubmit={(e) => addTransaction(e)}>
                                <Form.Group controlId="summary">
                                    <Form.Label>Summary</Form.Label>
                                    <Form.Control 
                                        type="text"   
                                        placeholder="Enter summary" 
                                        value={summary} 
                                        onChange={e => {setSummary(e.target.value)}} 
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="description">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control 
                                        type="text"  
                                        rows="3" 
                                        placeholder="Enter description" 
                                        value={description} 
                                        onChange={e => {setDescription(e.target.value)}} 
                                        required
                                    />
                                </Form.Group>
                                <Row>
                                    <Col xs={5} md={5}>
                                        <Form.Group controlId="amount">
                                            <Form.Label>Amount</Form.Label>
                                            <Form.Control 
                                                type="number" 
                                                rows="1" 
                                                value={amount} 
                                                onChange={e => {setAmount(e.target.value)}} 
                                                required
                                            />
                                        </Form.Group>
                                    </Col>
                                    <Col xs={7} md={7}>
                                        <Form.Group controlId="date">
                                            <Form.Label>Date</Form.Label>
                                            <DatePicker selected={startDate} onChange={date => setStartDate(date)} />
                                        </Form.Group>
                                    </Col>                                
                                </Row>
                                <Button className="bg-primary" type="submit">Submit</Button>
                                <Button className="bg-secondary" type="reset" onClick={e => resetForm()}>Reset</Button>                                
                            </Form>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="bg-secondary" onClick={handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>

    )
}
